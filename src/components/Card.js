import React from 'react';
import Label from './Label';
import Square from './Square';

class Card extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			color: "",
			colorList: ['A','B','C','D','E','F','0','1','2','3','4','5','6','7','8','9'],
		};
		this.changeColor = this.changeColor.bind(this);
		this.setColor = this.setColor.bind(this);
	}

	
	setColor() {
		let color = '#';
		let temp = 0;
		for(let i = 0; i < 6; i++) {
			temp = Math.floor(Math.random() * 16);
			color = color + this.state.colorList[temp];
		}
		this.setState({
			color:color
		});
	}

	changeColor(event) {
		this.setColor();
	}

	componentWillMount() {
		this.setColor();
	}

	render() {
		let cardStyle = {
			height: 200,
			width: 150,
			padding: 0,
			backgroundColor: "#FFF",
			WebkitFilter: "drop-shadow(0px 0px 5px #666)",
			filter: "drop-shadow(0px 0px 5px #666)"
		};

		return(
			<div style={cardStyle}>
			<Square color={this.state.color}></Square>
			<Label color={this.state.color} onColorChange={this.changeColor}></Label>
			</div>
			);
	}
}

export default Card;