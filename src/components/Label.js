import React from 'react';

class Label extends React.Component {
	constructor(props) {
		super(props);
		this.onChange = this.onChange.bind(this);
	}

	onChange(event) {
		this.props.onColorChange(event);
	}

	render() {
		let labelStyle = {
			fontFamily: "sans-serif",
			fontWeight: "bold",
			padding: 13,
			margin: 0
		};
		return(
			<button style={labelStyle} onClick={this.onChange}>{this.props.color}</button>
			);
	}
}

export default Label